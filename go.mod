module gitlab.com/hyperd/trader

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/google/uuid v1.1.2
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	gitlab.com/hyperd/trader/cockroachdb v0.0.0-20201122194644-a4ba68ab4cd7 // indirect
	gitlab.com/hyperd/trader/config v0.0.0-20201122165311-935a9345ce9b // indirect
	gitlab.com/hyperd/trader/implementation v0.0.0-20201122165311-935a9345ce9b // indirect
	gitlab.com/hyperd/trader/inmemory v0.0.0-20201122165311-935a9345ce9b // indirect
	gitlab.com/hyperd/trader/middleware v0.0.0-20201122194644-a4ba68ab4cd7 // indirect
	gitlab.com/hyperd/trader/transport/http v0.0.0-20201122194147-9b9461c9a8b0 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
)
