package trader

import (
	"context"
	"errors"

	"github.com/google/uuid"
)

// Response errors
var (
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrAlreadyExists   = errors.New("already exists")
	ErrNotFound        = errors.New("not found")
	ErrCmdRepository   = errors.New("unable to command repository")
	ErrQueryRepository = errors.New("unable to query repository")
	ErrParentNotFound  = errors.New("unable to retrieve the parent")
	ErrSymbolMismatch  = errors.New("the selecte symbol does not match the parent's")
)

// Service is a CRUD interface for a Trade.
type Service interface {
	PostTrade(ctx context.Context, t Trade) (string, error)
	GetTrade(ctx context.Context, ID uuid.UUID) (Trade, error)
	GetTrades(ctx context.Context) ([]Trade, error)
}
