module gitlab.com/hyperd/trader/middleware

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/google/uuid v1.1.2
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	gitlab.com/hyperd/trader v0.1.7
)
