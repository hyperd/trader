package middleware

import (
	"context"
	"strconv"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"

	"gitlab.com/hyperd/trader"
)

// LoggingMiddleware provides basic logging Middleware
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next trader.Service) trader.Service {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   trader.Service
	logger log.Logger
}

func (mw loggingMiddleware) PostTrade(ctx context.Context, t trader.Trade) (id string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "PostTrade", "trade", t.Symbol+" "+t.Side+" at "+strconv.FormatFloat(t.Price, 'f', 5, 64), "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.PostTrade(ctx, t)
}

func (mw loggingMiddleware) GetTrade(ctx context.Context, uuid uuid.UUID) (t trader.Trade, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetTrade", "uuid", uuid, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetTrade(ctx, uuid)
}

func (mw loggingMiddleware) GetTrades(ctx context.Context) (trades []trader.Trade, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetTrades", "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetTrades(ctx)
}
