package middleware

import (
	"context"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/google/uuid"
	"gitlab.com/hyperd/trader"
)

// TelegramMiddleware provides telegram notifications Middleware
func TelegramMiddleware(tgbot *tgbotapi.BotAPI) Middleware {
	return func(next trader.Service) trader.Service {
		return &telegramMiddleware{
			next:  next,
			tgbot: tgbot,
		}
	}
}

type telegramMiddleware struct {
	next  trader.Service
	tgbot *tgbotapi.BotAPI
}

func (mw telegramMiddleware) PostTrade(ctx context.Context, t trader.Trade) (id string, err error) {
	defer func(begin time.Time) {
		text := time.Now().Format(time.RFC850) + "\n\n" + "$" + t.Symbol + "\n" + t.Side + " @ " + strconv.FormatFloat(t.Price, 'f', 6, 64) + "\n\nleverage: " + strconv.Itoa(t.Leverage) + "x"

		msg := tgbotapi.NewMessage(-1001398844120, text)
		msg.ParseMode = tgbotapi.ModeHTML

		mw.tgbot.Send(msg)
	}(time.Now())
	return mw.next.PostTrade(ctx, t)
}

func (mw telegramMiddleware) GetTrade(ctx context.Context, uuid uuid.UUID) (t trader.Trade, err error) {

	return mw.next.GetTrade(ctx, uuid)
}

func (mw telegramMiddleware) GetTrades(ctx context.Context) (trades []trader.Trade, err error) {

	return mw.next.GetTrades(ctx)
}
