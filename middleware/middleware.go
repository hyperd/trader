package middleware

import "gitlab.com/hyperd/trader"

// Middleware describes the trader service (as opposed to endpoint) middleware.
type Middleware func(trader.Service) trader.Service
