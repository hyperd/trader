#!/bin/bash

set -Eeuo pipefail

COCKROACH_USER=hyperd

initdb() {
	# TODO: implement a more elegant way to wait for the cluster
	# to be up and running.
    echo "Wait for servers to be up"
    sleep 10

    HOSTPARAMS="--host roach1 --certs-dir /cockroach/certs --cert-principal-map=hyperd.sh:node,root:root"
    SQL="/cockroach/cockroach.sh sql $HOSTPARAMS"

    $SQL -e "CREATE DATABASE trader; CREATE USER IF NOT EXISTS ${COCKROACH_USER}; GRANT ALL ON DATABASE trader TO ${COCKROACH_USER};"
    $SQL -d trader -e "CREATE TABLE trades (
    	created_at TIMESTAMPTZ NULL,
    	updated_at TIMESTAMPTZ NULL,
    	deleted_at TIMESTAMPTZ NULL,
    	id UUID NOT NULL,
			parent UUID,
    	symbol STRING NOT NULL,
    	side STRING NOT NULL,
    	type STRING NOT NULL,
    	quantity DECIMAL NOT NULL,
    	leverage INT8 NOT NULL,
			price DECIMAL NOT NULL,
		CONSTRAINT \"primary\" PRIMARY KEY (id ASC),
    	INDEX idx_trades_deleted_at (deleted_at ASC),
    	FAMILY \"primary\" (created_at, updated_at, deleted_at, id, parent, symbol, side, type, quantity, leverage, price)
    );"
}

initdb
