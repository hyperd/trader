package cockroachdb

import (
	"context"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"gitlab.com/hyperd/trader"
)

type repository struct {
	db     *gorm.DB
	logger log.Logger
}

// New returns a concrete repository backed by CockroachDB
func New(db *gorm.DB, logger log.Logger) (trader.Repository, error) {
	// return  repository
	return &repository{
		db:     db,
		logger: log.With(logger, "rep", "cockroachdb"),
	}, nil
}

// Functions of type `txnFunc` are passed as arguments to our
// `runTransaction` wrapper that handles transaction retries for us
// (see implementation below).
type txnFunc func(*gorm.DB) error

// This function is used for testing the transaction retry loop.  It
// can be deleted from production code.
var forceRetryLoop txnFunc = func(db *gorm.DB) error {

	// The first statement in a transaction can be retried transparently
	// on the server, so we need to add a dummy statement so that our
	// force_retry statement isn't the first one.
	if err := db.Exec("SELECT now()").Error; err != nil {
		return err
	}
	// Used to force a transaction retry.  Can only be run as the
	// 'root' user.
	if err := db.Exec("SELECT crdb_internal.force_retry('1s'::INTERVAL)").Error; err != nil {
		return err
	}
	return nil
}

func validInput(id uuid.UUID) bool {
	_, err := uuid.Parse(id.String())

	if err != nil {
		return false
	}

	return true
}

func (repo *repository) PostTrade(ctx context.Context, trade trader.Trade) (string, error) {
	// Run a transaction to sync the query model.
	id := uuid.New()
	trade.ID = id

	// Determine if the trade has a parent and the symbols are coherent
	if trade.Parent != uuid.Nil {
		var parent = trader.Trade{}

		if err := repo.db.Where("id = ?", trade.Parent).Where("symbol = ?", trade.Symbol).First(&parent).Error; err != nil {
			return err.Error(), trader.ErrParentNotFound
		}
	}

	if err := repo.db.Create(&trader.Trade{
		ID:       trade.ID,
		Parent:   trade.Parent,
		Symbol:   trade.Symbol,
		Side:     trade.Side,
		Type:     trade.Type,
		Quantity: trade.Quantity,
		Leverage: trade.Leverage,
		Price:    trade.Price}).Error; err != nil {

		return err.Error(), err
	}

	return id.String(), nil
}

func (repo *repository) GetTrade(ctx context.Context, id uuid.UUID) (trader.Trade, error) {
	var trade = trader.Trade{}

	if !validInput(id) {
		return trade, trader.ErrNotFound
	}

	if err := repo.db.Where("id = ?", id).First(&trade).Error; err != nil {
		return trade, trader.ErrNotFound
	}

	return trade, nil
}

// You should NEVER try to expose this method in any transport
// Trades cannot be removed from an exchange.
func (repo *repository) DeleteTrade(ctx context.Context, id uuid.UUID) (string, error) {

	if !validInput(id) {
		return uuid.New().String(), trader.ErrNotFound
	}

	tx := repo.db.Begin()

	if err := tx.Where("id = ?", id).Delete(&trader.Trade{}).Error; err != nil {
		tx.Rollback()
		return id.String(), err
	}

	tx.Commit()

	return id.String(), nil
}

func (repo *repository) GetTrades(ctx context.Context) ([]trader.Trade, error) {
	var trades []trader.Trade

	repo.db.Find(&trades).Order("id ASC").Group("parent")

	return trades, nil
}
