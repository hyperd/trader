module gitlab.com/hyperd/trader/cockroachdb

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	gitlab.com/hyperd/trader v0.1.7
)
