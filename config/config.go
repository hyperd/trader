package config

// TraderConfig exported
type TraderConfig struct {
	Paths    PathsConfig
	Server   ServerConfig
	Database DatabaseConfig
	Telegram TelegramConfig
}

// PathsConfig configuration exported
type PathsConfig struct {
	TLSCert string `mapstructure:"tls_cert"`
	TLSKey  string `mapstructure:"tls_key"`
}

// ServerConfig configuration exported
type ServerConfig struct {
	HTTPPort int `mapstructure:"http_port"`
	TLSPort  int `mapstructure:"tls_port"`
}

// DatabaseConfig configuration exported
type DatabaseConfig struct {
	BackendType      string `mapstructure:"backend_type"`
	ConnectionString string `mapstructure:"connection_string"`
}

// TelegramConfig configuration exported
type TelegramConfig struct {
	APIToken string `mapstructure:"api_token"`
	ChatID   int64  `mapstructure:"chat_id"`
}
