FROM alpine:3.12.1

RUN apk update ; \
	apk upgrade ; \
	rm -rf /var/cache/apk/* \
	export RANDOM_PASSWORD=`tr -dc A-Za-z0-9 < /dev/urandom | head -c71` ; \
	echo "root:$RANDOM_PASSWORD" | chpasswd ; \
	unset RANDOM_PASSWORD ; \
	passwd -l root

RUN set -ex; \
	apkArch="$(apk --print-arch)"; \
	case "$apkArch" in \
	armhf) arch='armv6' ;; \
	aarch64) arch='arm64' ;; \
	x86_64) arch='amd64' ;; \
	*) echo >&2 "error: unsupported architecture: $apkArch"; exit 1 ;; \
	esac; \
	wget --quiet -O /usr/local/bin/trader "https://gitlab.com/hyperd/trader/raw/master/releases/trader-linux-$arch"; \
	chmod +x /usr/local/bin/trader

RUN adduser -D -g '' appuser

USER appuser

EXPOSE 3000 8443

ENTRYPOINT ["trader"]

# Metadata
LABEL org.opencontainers.image.vendor="Hyperd" \
	org.opencontainers.image.url="https://hyperd.sh" \
	org.opencontainers.image.title="Trader" \
	org.opencontainers.image.description="Trader: Automated Trading Bot for Binance Futures" \
	org.opencontainers.image.version="v0.5" \
	org.opencontainers.image.documentation="https://gitlab.com/hyperd/trader/blob/master/README.md"
