#!/bin/bash

set -Eeo pipefail

GREEN='\033[1;32m'
NC='\033[0m' # No Color

report () {
	echo -e "
${GREEN}[OK]${NC} successfully generated the self-signed certs for trader.hyperd.sh
"
}

generate_certs () {
  tls_secrets_folder="$(pwd)/.secrets/tls"
  # create a private key and a self signed certificate (remember that old skool 2048 bit as Google load balancers don't like the stronger RSA-4096)
  pushd "$tls_secrets_folder" || return
    if [[ ! -f $tls_secrets_folder/trader.hyperd.sh.key ]] || [[ ! -f $tls_secrets_folder/trader.hyperd.sh.crt ]]; then
      echo -e "${GREEN}[INFO]${NC} generating self-signed certs for trader.hyperd.sh"
      openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
      -subj "/C=NL/ST=Amsterdam/L=Amsterdam/O=hyperd/CN=trader.hyperd.sh" \
      -keyout trader.hyperd.sh.key -out trader.hyperd.sh.crt
    fi

    # create a strong Diffie-Hellman group, used in negotiating Perfect Forward Secrecy with clients
    if [[ ! -f $tls_secrets_folder/dhparam.pem ]]; then
      echo -e "${GREEN}[INFO]${NC} generating dhparam"
      openssl dhparam -out dhparam.pem 2048
    fi
  popd || return
}

pushd "$(git rev-parse --show-toplevel)"  || return
  generate_certs && report
popd || return