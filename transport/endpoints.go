package transport

import (
	"context"

	"gitlab.com/hyperd/trader"

	"github.com/go-kit/kit/endpoint"
	"github.com/google/uuid"
)

// Endpoints collects all of the endpoints part of Trader
type Endpoints struct {
	PostTradeEndpoint    endpoint.Endpoint
	GetTradesEndpoint    endpoint.Endpoint
	GetTradeEndpoint     endpoint.Endpoint
	GetAPIStatusEndpoint endpoint.Endpoint
}

// MakeServerEndpoints returns an Endpoints struct where each endpoint invokes
// the corresponding method on the provided trader.Service. Useful in a trader.Service
// server.
func MakeServerEndpoints(s trader.Service) Endpoints {
	return Endpoints{
		PostTradeEndpoint:    MakePostTradeEndpoint(s),
		GetTradesEndpoint:    MakeGetTradesEndpoint(s),
		GetTradeEndpoint:     MakeGetTradeEndpoint(s),
		GetAPIStatusEndpoint: MakeGetAPIStatusEndpoint(),
	}
}

// MakePostTradeEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakePostTradeEndpoint(s trader.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(PostTradeRequest)
		id, e := s.PostTrade(ctx, req.Trade)
		return PostTradeResponse{ID: id, Err: e}, nil
	}
}

// MakeGetTradesEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetTradesEndpoint(s trader.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		t, e := s.GetTrades(ctx)
		return GetTradesResponse{Trades: t, Err: e}, nil
	}
}

// MakeGetTradeEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetTradeEndpoint(s trader.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(GetTradeRequest)
		t, e := s.GetTrade(ctx, req.ID)
		return GetTradeResponse{Trade: t, Err: e}, nil
	}
}

// MakeGetAPIStatusEndpoint returns an endpoint via the passed service.
// Primarily useful in a server.
func MakeGetAPIStatusEndpoint() endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {

		return GetAPIStatusResponse{Status: "Healthy"}, nil
	}
}

// We have two options to return errors from the business logic.
//
// We could return the error via the endpoint itself. That makes certain things
// a little bit easier, like providing non-200 HTTP responses to the client. But
// Go kit assumes that endpoint errors are (or may be treated as)
// transport-domain errors. For example, an endpoint error will count against a
// circuit breaker error count.
//
// Therefore, it's often better to return service (business logic) errors in the
// response object. This means we have to do a bit more work in the HTTP
// response encoder to detect e.g. a not-found error and provide a proper HTTP
// status code. That work is done with the errorer interface, in transport.go.
// Response types that may contain business-logic errors implement that
// interface.

// PostTradeRequest request object
type PostTradeRequest struct {
	Trade trader.Trade `json:"trade,omitempty"`
}

// PostTradeResponse response object
type PostTradeResponse struct {
	ID  string `json:"id,omitempty"`
	Err error  `json:"err,omitempty"`
}

func (r PostTradeResponse) error() error { return r.Err }

// GetTradesRequest struct
type GetTradesRequest struct{}

// GetTradesResponse response object
type GetTradesResponse struct {
	Trades []trader.Trade `json:"trades,omitempty"`
	Err    error          `json:"err,omitempty"`
}

func (r GetTradesResponse) error() error { return r.Err }

// GetTradeRequest request object
type GetTradeRequest struct {
	ID uuid.UUID
}

// GetTradeResponse response object
type GetTradeResponse struct {
	Trade trader.Trade `json:"trade,omitempty"`
	Err   error        `json:"err,omitempty"`
}

func (r GetTradeResponse) error() error { return r.Err }

// GetAPIStatusRequest request object
type GetAPIStatusRequest struct{}

// GetAPIStatusResponse response object
type GetAPIStatusResponse struct {
	Status string `json:"status,omitempty"`
	Err    error  `json:"err,omitempty"`
}

func (r GetAPIStatusResponse) error() error { return r.Err }
