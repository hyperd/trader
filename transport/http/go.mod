module gitlab.com/hyperd/trader/transport/http

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	gitlab.com/hyperd/trader v0.1.7
	gitlab.com/hyperd/trader/transport v0.0.0-20201122165311-935a9345ce9b
)
