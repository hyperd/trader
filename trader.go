package trader

import (
	"context"

	"github.com/google/uuid"
)

// Trade represents a single Binance trade.
type Trade struct {
	// gorm.Model
	ID       uuid.UUID `json:"uuid,omitempty" gorm:"primary_key"`
	Parent   uuid.UUID `json:"parent,omoniempty" valid:"optional"`
	Symbol   string    `json:"symbol,omitempty" valid:"required"`
	Side     string    `json:"side,omitempty" valid:"required,in(BUY|SELL)"`
	Type     string    `json:"type,omitempty" valid:"required,in(LIMIT|MARKET)"`
	Quantity float64   `json:"quantity,omitempty" valid:"required,float"`
	Leverage int       `json:"leverage,omitempty" valid:"required,numeric,range(0|50)"`
	Price    float64   `json:"price,omitempty" valid:"required,float"`
}

// Repository describes the persistence on trade model
type Repository interface {
	PostTrade(ctx context.Context, t Trade) (string, error)
	GetTrade(ctx context.Context, ID uuid.UUID) (Trade, error)
	GetTrades(ctx context.Context) ([]Trade, error)
}
