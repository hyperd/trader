package binance

// OrderStatus represents order status enum.
type OrderStatus string

// OrderType represents order type enum.
type OrderType string

// OrderSide represents order side enum.
type OrderSide string

var (
	// StatusNew - new order
	StatusNew = OrderStatus("NEW")
	// StatusPartiallyFilled - partiallu filled order
	StatusPartiallyFilled = OrderStatus("PARTIALLY_FILLED")
	// StatusFilled - filled order
	StatusFilled = OrderStatus("FILLED")
	// StatusCancelled - cancelled order
	StatusCancelled = OrderStatus("CANCELED")
	// StatusPendingCancel - pending order
	StatusPendingCancel = OrderStatus("PENDING_CANCEL")
	// StatusRejected - rejected order
	StatusRejected = OrderStatus("REJECTED")
	// StatusExpired - expired order
	StatusExpired = OrderStatus("EXPIRED")

	// TypeLimit - limit order type
	TypeLimit = OrderType("LIMIT")
	// TypeMarket - market order type
	TypeMarket = OrderType("MARKET")

	// SideBuy - BUY order
	SideBuy = OrderSide("BUY")
	// SideSell - SELL order
	SideSell = OrderSide("SELL")
)
