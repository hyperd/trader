package binance

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"github.com/pkg/errors"
)

func (as *apiService) Ping() error {
	params := make(map[string]string)
	response, err := as.request("GET", "api/v1/ping", params, false, false)
	if err != nil {
		return err
	}
	fmt.Printf("%#v\n", response.StatusCode)
	return nil
}

func (as *apiService) Time() (time.Time, error) {
	params := make(map[string]string)
	res, err := as.request("GET", "api/v1/time", params, false, false)
	if err != nil {
		return time.Time{}, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return time.Time{}, errors.Wrap(err, "unable to read response from Time")
	}
	defer res.Body.Close()
	var rawTime struct {
		ServerTime string `json:"serverTime"`
	}
	if err := json.Unmarshal(textRes, &rawTime); err != nil {
		return time.Time{}, errors.Wrap(err, "timeResponse unmarshal failed")
	}
	t, err := timeFromUnixTimestampFloat(rawTime)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}
