package binance

// Interval represents interval enum.
type Interval string

var (
	// Minute interval
	Minute = Interval("1m")
	// ThreeMinutes interval
	ThreeMinutes = Interval("3m")
	// FiveMinutes interval
	FiveMinutes = Interval("5m")
	// FifteenMinutes interval
	FifteenMinutes = Interval("15m")
	// ThirtyMinutes interval
	ThirtyMinutes = Interval("30m")
	// Hour interval
	Hour = Interval("1h")
	// TwoHours interval
	TwoHours = Interval("2h")
	// FourHours interval
	FourHours = Interval("4h")
	// SixHours interval
	SixHours = Interval("6h")
	// EightHours interval
	EightHours = Interval("8h")
	// TwelveHours interval
	TwelveHours = Interval("12h")
	// Day interval
	Day = Interval("1d")
	// ThreeDays interval
	ThreeDays = Interval("3d")
	// Week interval
	Week = Interval("1w")
	// Month interval
	Month = Interval("1M")
)

// TimeInForce represents timeInForce enum.
type TimeInForce string

var (
	// GTC Good Till Cancelled
	GTC = TimeInForce("GTC")
	// IOC Immediate Or Cancel
	IOC = TimeInForce("IOC")
)
