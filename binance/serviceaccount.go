package binance

import (
	"encoding/json"
	"io/ioutil"
	"strconv"

	"github.com/pkg/errors"
)

type rawExecutedOrder struct {
	Symbol        string  `json:"symbol"`
	OrderID       int     `json:"orderId"`
	ClientOrderID string  `json:"clientOrderId"`
	Price         string  `json:"price"`
	OrigQty       string  `json:"origQty"`
	ExecutedQty   string  `json:"executedQty"`
	Status        string  `json:"status"`
	TimeInForce   string  `json:"timeInForce"`
	Type          string  `json:"type"`
	Side          string  `json:"side"`
	StopPrice     string  `json:"stopPrice"`
	IcebergQty    string  `json:"icebergQty"`
	Time          float64 `json:"time"`
}

func (as *apiService) NewOrder(or NewOrderRequest) (*ProcessedOrder, error) {
	params := make(map[string]string)
	params["symbol"] = or.Symbol
	params["side"] = string(or.Side)
	params["type"] = string(or.Type)
	params["timeInForce"] = string(or.TimeInForce)
	params["quantity"] = strconv.FormatFloat(or.Quantity, 'f', 10, 64)
	params["price"] = strconv.FormatFloat(or.Price, 'f', 10, 64)
	params["timestamp"] = strconv.FormatInt(unixMillis(or.Timestamp), 10)
	if or.NewClientOrderID != "" {
		params["newClientOrderId"] = or.NewClientOrderID
	}
	if or.StopPrice != 0 {
		params["stopPrice"] = strconv.FormatFloat(or.StopPrice, 'f', 10, 64)
	}
	if or.IcebergQty != 0 {
		params["icebergQty"] = strconv.FormatFloat(or.IcebergQty, 'f', 10, 64)
	}

	res, err := as.request("POST", "api/v3/order", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from Ticker/24hr")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawOrder := struct {
		Symbol        string  `json:"symbol"`
		OrderID       int64   `json:"orderId"`
		ClientOrderID string  `json:"clientOrderId"`
		TransactTime  float64 `json:"transactTime"`
	}{}
	if err := json.Unmarshal(textRes, &rawOrder); err != nil {
		return nil, errors.Wrap(err, "rawOrder unmarshal failed")
	}

	t, err := timeFromUnixTimestampFloat(rawOrder.TransactTime)
	if err != nil {
		return nil, err
	}

	return &ProcessedOrder{
		Symbol:        rawOrder.Symbol,
		OrderID:       rawOrder.OrderID,
		ClientOrderID: rawOrder.ClientOrderID,
		TransactTime:  t,
	}, nil
}

func (as *apiService) NewOrderTest(or NewOrderRequest) error {
	params := make(map[string]string)
	params["symbol"] = or.Symbol
	params["side"] = string(or.Side)
	params["type"] = string(or.Type)
	params["timeInForce"] = string(or.TimeInForce)
	params["quantity"] = strconv.FormatFloat(or.Quantity, 'f', 10, 64)
	params["price"] = strconv.FormatFloat(or.Price, 'f', 10, 64)
	params["timestamp"] = strconv.FormatInt(unixMillis(or.Timestamp), 10)
	if or.NewClientOrderID != "" {
		params["newClientOrderId"] = or.NewClientOrderID
	}
	if or.StopPrice != 0 {
		params["stopPrice"] = strconv.FormatFloat(or.StopPrice, 'f', 10, 64)
	}
	if or.IcebergQty != 0 {
		params["icebergQty"] = strconv.FormatFloat(or.IcebergQty, 'f', 10, 64)
	}

	res, err := as.request("POST", "api/v3/order/test", params, true, true)
	if err != nil {
		return err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return errors.Wrap(err, "unable to read response from Ticker/24hr")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return as.handleError(textRes)
	}
	return nil
}

func (as *apiService) QueryOrder(qor QueryOrderRequest) (*ExecutedOrder, error) {
	params := make(map[string]string)
	params["symbol"] = qor.Symbol
	params["timestamp"] = strconv.FormatInt(unixMillis(qor.Timestamp), 10)
	if qor.OrderID != 0 {
		params["orderId"] = strconv.FormatInt(qor.OrderID, 10)
	}
	if qor.OrigClientOrderID != "" {
		params["origClientOrderId"] = qor.OrigClientOrderID
	}
	if qor.RecvWindow != 0 {
		params["recvWindow"] = strconv.FormatInt(recvWindow(qor.RecvWindow), 10)
	}

	res, err := as.request("GET", "api/v3/order", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from order.get")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawOrder := &rawExecutedOrder{}
	if err := json.Unmarshal(textRes, rawOrder); err != nil {
		return nil, errors.Wrap(err, "rawOrder unmarshal failed")
	}

	eo, err := executedOrderFromRaw(rawOrder)
	if err != nil {
		return nil, err
	}
	return eo, nil
}

func (as *apiService) CancelOrder(cor CancelOrderRequest) (*CanceledOrder, error) {
	params := make(map[string]string)
	params["symbol"] = cor.Symbol
	params["timestamp"] = strconv.FormatInt(unixMillis(cor.Timestamp), 10)
	if cor.OrderID != 0 {
		params["orderId"] = strconv.FormatInt(cor.OrderID, 10)
	}
	if cor.OrigClientOrderID != "" {
		params["origClientOrderId"] = cor.OrigClientOrderID
	}
	if cor.NewClientOrderID != "" {
		params["newClientOrderId"] = cor.NewClientOrderID
	}
	if cor.RecvWindow != 0 {
		params["recvWindow"] = strconv.FormatInt(recvWindow(cor.RecvWindow), 10)
	}

	res, err := as.request("DELETE", "api/v3/order", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from order.delete")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawCanceledOrder := struct {
		Symbol            string `json:"symbol"`
		OrigClientOrderID string `json:"origClientOrderId"`
		OrderID           int64  `json:"orderId"`
		ClientOrderID     string `json:"clientOrderId"`
	}{}
	if err := json.Unmarshal(textRes, &rawCanceledOrder); err != nil {
		return nil, errors.Wrap(err, "cancelOrder unmarshal failed")
	}

	return &CanceledOrder{
		Symbol:            rawCanceledOrder.Symbol,
		OrigClientOrderID: rawCanceledOrder.OrigClientOrderID,
		OrderID:           rawCanceledOrder.OrderID,
		ClientOrderID:     rawCanceledOrder.ClientOrderID,
	}, nil
}

func (as *apiService) OpenOrders(oor OpenOrdersRequest) ([]*ExecutedOrder, error) {
	params := make(map[string]string)
	params["symbol"] = oor.Symbol
	params["timestamp"] = strconv.FormatInt(unixMillis(oor.Timestamp), 10)
	if oor.RecvWindow != 0 {
		params["recvWindow"] = strconv.FormatInt(recvWindow(oor.RecvWindow), 10)
	}

	res, err := as.request("GET", "api/v3/openOrders", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from openOrders.get")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawOrders := []*rawExecutedOrder{}
	if err := json.Unmarshal(textRes, &rawOrders); err != nil {
		return nil, errors.Wrap(err, "openOrders unmarshal failed")
	}

	var eoc []*ExecutedOrder
	for _, rawOrder := range rawOrders {
		eo, err := executedOrderFromRaw(rawOrder)
		if err != nil {
			return nil, err
		}
		eoc = append(eoc, eo)
	}

	return eoc, nil
}

func (as *apiService) AllOrders(aor AllOrdersRequest) ([]*ExecutedOrder, error) {
	params := make(map[string]string)
	params["symbol"] = aor.Symbol
	params["timestamp"] = strconv.FormatInt(unixMillis(aor.Timestamp), 10)
	if aor.OrderID != 0 {
		params["orderId"] = strconv.FormatInt(aor.OrderID, 10)
	}
	if aor.Limit != 0 {
		params["limit"] = strconv.Itoa(aor.Limit)
	}
	if aor.RecvWindow != 0 {
		params["recvWindow"] = strconv.FormatInt(recvWindow(aor.RecvWindow), 10)
	}

	res, err := as.request("GET", "api/v3/allOrders", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from allOrders.get")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawOrders := []*rawExecutedOrder{}
	if err := json.Unmarshal(textRes, &rawOrders); err != nil {
		return nil, errors.Wrap(err, "allOrders unmarshal failed")
	}

	var eoc []*ExecutedOrder
	for _, rawOrder := range rawOrders {
		eo, err := executedOrderFromRaw(rawOrder)
		if err != nil {
			return nil, err
		}
		eoc = append(eoc, eo)
	}

	return eoc, nil
}

func (as *apiService) Account(ar AccountRequest) (*Account, error) {
	params := make(map[string]string)
	params["timestamp"] = strconv.FormatInt(unixMillis(ar.Timestamp), 10)
	if ar.RecvWindow != 0 {
		params["recvWindow"] = strconv.FormatInt(recvWindow(ar.RecvWindow), 10)
	}

	res, err := as.request("GET", "api/v3/account", params, true, true)
	if err != nil {
		return nil, err
	}
	textRes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read response from account.get")
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, as.handleError(textRes)
	}

	rawAccount := struct {
		MakerCommision   int64 `json:"makerCommision"`
		TakerCommission  int64 `json:"takerCommission"`
		BuyerCommission  int64 `json:"buyerCommission"`
		SellerCommission int64 `json:"sellerCommission"`
		CanTrade         bool  `json:"canTrade"`
		CanWithdraw      bool  `json:"canWithdraw"`
		CanDeposit       bool  `json:"canDeposit"`
		Balances         []struct {
			Asset  string `json:"asset"`
			Free   string `json:"free"`
			Locked string `json:"locked"`
		}
	}{}
	if err := json.Unmarshal(textRes, &rawAccount); err != nil {
		return nil, errors.Wrap(err, "rawAccount unmarshal failed")
	}

	acc := &Account{
		MakerCommision:  rawAccount.MakerCommision,
		TakerCommision:  rawAccount.TakerCommission,
		BuyerCommision:  rawAccount.BuyerCommission,
		SellerCommision: rawAccount.SellerCommission,
		CanTrade:        rawAccount.CanTrade,
		CanWithdraw:     rawAccount.CanWithdraw,
		CanDeposit:      rawAccount.CanDeposit,
	}
	for _, b := range rawAccount.Balances {
		f, err := floatFromString(b.Free)
		if err != nil {
			return nil, err
		}
		l, err := floatFromString(b.Locked)
		if err != nil {
			return nil, err
		}
		acc.Balances = append(acc.Balances, &Balance{
			Asset:  b.Asset,
			Free:   f,
			Locked: l,
		})
	}

	return acc, nil
}

func executedOrderFromRaw(reo *rawExecutedOrder) (*ExecutedOrder, error) {
	price, err := strconv.ParseFloat(reo.Price, 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.CloseTime")
	}
	origQty, err := strconv.ParseFloat(reo.OrigQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.OrigQty")
	}
	execQty, err := strconv.ParseFloat(reo.ExecutedQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.ExecutedQty")
	}
	stopPrice, err := strconv.ParseFloat(reo.StopPrice, 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.StopPrice")
	}
	icebergQty, err := strconv.ParseFloat(reo.IcebergQty, 64)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.IcebergQty")
	}
	t, err := timeFromUnixTimestampFloat(reo.Time)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse Order.CloseTime")
	}

	return &ExecutedOrder{
		Symbol:        reo.Symbol,
		OrderID:       reo.OrderID,
		ClientOrderID: reo.ClientOrderID,
		Price:         price,
		OrigQty:       origQty,
		ExecutedQty:   execQty,
		Status:        OrderStatus(reo.Status),
		TimeInForce:   TimeInForce(reo.TimeInForce),
		Type:          OrderType(reo.Type),
		Side:          OrderSide(reo.Side),
		StopPrice:     stopPrice,
		IcebergQty:    icebergQty,
		Time:          t,
	}, nil
}
