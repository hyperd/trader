module gitlab.com/hyperd/trader/binance

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/pkg/errors v0.9.1
)
