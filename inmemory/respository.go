package inmemory

import (
	"context"
	"errors"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"gitlab.com/hyperd/trader"
)

// Response errors
var (
	ErrInconsistentID = errors.New("inconsistent ID")
	ErrAlreadyExists  = errors.New("already exists")
	ErrNotFound       = errors.New("not found")
)

type repository struct {
	mtx    sync.RWMutex
	m      map[string]trader.Trade
	logger log.Logger
}

// NewInmemService returns an in-memory storage
func NewInmemService(logger log.Logger) (trader.Repository, error) {
	return &repository{
		m:      map[string]trader.Trade{},
		logger: log.With(logger, "repository", "inmemory"),
	}, nil
}

func (r *repository) PostTrade(ctx context.Context, t trader.Trade) (string, error) {
	r.mtx.Lock()
	defer r.mtx.Unlock()

	id := uuid.New()

	t.ID = id

	if _, ok := r.m[t.ID.String()]; ok {
		return "", ErrAlreadyExists // POST = create, don't overwrite
	}
	r.m[t.ID.String()] = t
	return id.String(), nil
}

func (r *repository) GetTrades(ctx context.Context) ([]trader.Trade, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()

	p := []trader.Trade{}
	for _, value := range r.m {
		p = append(p, value)
	}

	if p == nil {
		return []trader.Trade{}, ErrNotFound
	}
	return p, nil
}

func (r *repository) GetTrade(ctx context.Context, uuid uuid.UUID) (trader.Trade, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	t, ok := r.m[uuid.String()]
	if !ok {
		return trader.Trade{}, ErrNotFound
	}
	return t, nil
}
