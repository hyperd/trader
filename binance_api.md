# Binance API breakdown

## Set wallet balance to check against leverage and position

`GET /fapi/v2/balance?timestamp={{timestamp}}&signature={{signature}}`

**Response:**

```json
[
    {
        "accountAlias": "SgsR",                 // unique account code
        "asset": "USDT",                        // asset name
        "balance": "122607.35137903",           // wallet balance
        "crossWalletBalance": "23.72469206",    // crossed wallet balance
        "crossUnPnl": "0.00000000",             // unrealized profit of crossed positions
        "availableBalance": "23.72469206",      // available balance
        "maxWithdrawAmount": "23.72469206"      // maximum amount for transfer out
    }
]
```

---

## Check 5x leverage and 8% per position

`GET /fapi/v2/positionRisk?timestamp={{timestamp}}&signature={{signature}}`

**Response:**

```json
[
    {
        "entryPrice": "0.00000",
        "marginType": "isolated", 
        "isAutoAddMargin": "false",
        "isolatedMargin": "0.00000000", 
        "leverage": "10", 
        "liquidationPrice": "0", 
        "markPrice": "6679.50671178",   
        "maxNotionalValue": "20000000", 
        "positionAmt": "0.000", 
        "symbol": "BTCUSDT", 
        "unRealizedProfit": "0.00000000", 
        "positionSide": "BOTH",
    },
    {
        "entryPrice": "6563.66500", 
        "marginType": "isolated", 
        "isAutoAddMargin": "false",
        "isolatedMargin": "15517.54150468",
        "leverage": "10",
        "liquidationPrice": "5930.78",
        "markPrice": "6679.50671178",   
        "maxNotionalValue": "20000000", 
        "positionAmt": "20.000", 
        "symbol": "BTCUSDT", 
        "unRealizedProfit": "2316.83423560"
        "positionSide": "LONG", 
    },
    {
        "entryPrice": "0.00000",
        "marginType": "isolated", 
        "isAutoAddMargin": "false",
        "isolatedMargin": "5413.95799991", 
        "leverage": "10", 
        "liquidationPrice": "7189.95", 
        "markPrice": "6679.50671178",   
        "maxNotionalValue": "20000000", 
        "positionAmt": "-10.000", 
        "symbol": "BTCUSDT",
        "unRealizedProfit": "-1156.46711780" 
        "positionSide": "SHORT",
    }
]
```

---

## Deposits

To check no deposits were added after challenge start date:
Pass startTime as date of challenge. Disqualified if length of depositList >= 1

`GET /wapi/v3/depositHistory.html?timestamp={{timestamp}}&signature={{signature}}`

**Response:**

```json
{
    "depositList": [
        {
            "insertTime": 1508198532000,
            "amount": 0.04670582,
            "asset": "ETH",
            "address": "0x6915f16f8791d0a1cc2bf47c13a6b2a92000504b",    // deposit address
            "txId": "0xdf33b22bdb2b28b1f75ccd201a4a4m6e7g83jy5fc5d5a9d1340961598cfcb0a1",
            "status": 1
        },
        {
            "insertTime": 1508298532000,
            "amount": 1000,
            "asset": "XMR",
            "address": "463tWEBn5XZJSxLU34r6g7h8jtxuNcDbjLSjkn3XAXHCbLrTTErJrBWYgHJQyrCwkNgYvyV3z8zctJLPCZy24jvb3NiTcTJ",
            "addressTag": "342341222",
            "txId": "b3c6219639c8ae3f9cf010cdc24fw7f7yt8j1e063f9b4bd1a05cb44c4b6e2509",
            "status": 1
        }
    ],
    "success": true
}
```
