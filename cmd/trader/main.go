package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/qor/validations"
	"github.com/spf13/viper"
	trader "gitlab.com/hyperd/trader"
	"gitlab.com/hyperd/trader/cockroachdb"
	"gitlab.com/hyperd/trader/config"
	tradersvc "gitlab.com/hyperd/trader/implementation"
	"gitlab.com/hyperd/trader/inmemory"
	"gitlab.com/hyperd/trader/middleware"
	httptransport "gitlab.com/hyperd/trader/transport/http"
	"golang.org/x/net/http2"
)

func exit(logger log.Logger, err error) {
	level.Error(logger).Log("exit", err)
	os.Exit(-1)
}

func initViper(logger log.Logger) {

	level.Info(logger).Log("msg", "initializing viper")

	// setup viper and populate the trader config obj
	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".config/prod/")

	if err := viper.ReadInConfig(); err != nil { // Find and read the config file
		exit(logger, err)
	}
}

func main() {
	// setup the logger
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.NewSyncLogger(logger)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)

		logger = log.With(logger,
			"svc", "trader",
			"ts", log.DefaultTimestampUTC,
			"caller", log.DefaultCaller,
		)
	}

	initViper(logger)

	var conf config.TraderConfig

	err := viper.Unmarshal(&conf)
	if err != nil {
		exit(logger, err)
	}

	// cli flags defaults
	var (
		httpAddr     = flag.String("http.addr", ":"+strconv.Itoa(conf.Server.HTTPPort), "HTTP listen address")
		httpsAddr    = flag.String("https.addr", ":"+strconv.Itoa(conf.Server.TLSPort), "HTTPS listen address")
		databaseType = flag.String("database.type", conf.Database.BackendType, "Backend type")
	)
	flag.Parse()

	level.Info(logger).Log("msg", "service started")

	defer level.Info(logger).Log("msg", "service ended")

	tgbot, err := tgbotapi.NewBotAPI(conf.Telegram.APIToken)
	go func() {

		if err != nil {
			exit(logger, err)
		}

		level.Info(logger).Log("telegram-bot", tgbot.Self.FirstName)

		text := time.Now().Format(time.RFC850) + "\n" + "<b>" + tgbot.Self.FirstName + "</b>" + " is connected."

		msg := tgbotapi.NewMessage(conf.Telegram.ChatID, text)
		msg.ParseMode = tgbotapi.ModeHTML

		tgbot.Send(msg)
	}()

	selectedBackend := *databaseType // safe to dereference the *string
	isInMemory := (selectedBackend == "inmemory")

	var db *gorm.DB
	{
		if !isInMemory {
			var err error

			db, err = gorm.Open("postgres", conf.Database.ConnectionString)

			if err != nil {
				exit(logger, err)
			}
			defer db.Close()

			// Set to `true` and GORM will print out all DB queries.
			db.LogMode(true)

			// Disable table name's pluralization globally
			db.SingularTable(false)
			db.AutoMigrate(&trader.Trade{})

			// Validations uses GORM callbacks to handle validations
			validations.RegisterCallbacks(db)
		}
	}

	var svc trader.Service
	{
		if isInMemory {
			level.Info(logger).Log("backend", "database", "type", "inmemory")

			repository, err := inmemory.NewInmemService(logger)
			if err != nil {
				exit(logger, err)
			}
			svc = tradersvc.NewService(repository, logger)
		} else {
			level.Info(logger).Log("backend", "database", "type", "cockroachdb")

			repository, err := cockroachdb.New(db, logger)
			if err != nil {
				exit(logger, err)
			}
			svc = tradersvc.NewService(repository, logger)
		}
		// Service middleware: Logging
		svc = middleware.LoggingMiddleware(logger)(svc)

		// Service middleware: Telegram
		svc = middleware.TelegramMiddleware(tgbot)(svc)

	}

	var h http.Handler
	{
		h = httptransport.MakeHTTPHandler(svc, log.With(logger, "component", "HTTP"))
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		logger.Log("transport", "HTTP", "addr", *httpAddr)
		errs <- http.ListenAndServe(*httpAddr, h)
	}()

	go func() {
		var httpServer = http.Server{
			Addr:    *httpsAddr,
			Handler: h,
		}

		var http2Server = http2.Server{}
		_ = http2.ConfigureServer(&httpServer, &http2Server)
		logger.Log("transport", "HTTPS", "addr", *httpsAddr)
		errs <- httpServer.ListenAndServeTLS(conf.Paths.TLSCert, conf.Paths.TLSKey)
	}()

	logger.Log("exit", <-errs)
}
