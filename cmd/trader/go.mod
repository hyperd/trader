module gitlab.com/hyperd/trader/cmd/trader

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/jinzhu/gorm v1.9.16
	github.com/qor/qor v0.0.0-20200729071734-d587cffbbb93 // indirect
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46
	github.com/spf13/viper v1.7.1
	gitlab.com/hyperd/trader v0.1.7
	gitlab.com/hyperd/trader/cockroachdb v0.0.0-20201122165311-935a9345ce9b
	gitlab.com/hyperd/trader/config v0.0.0-20201122165311-935a9345ce9b
	gitlab.com/hyperd/trader/implementation v0.0.0-20201122165311-935a9345ce9b
	gitlab.com/hyperd/trader/inmemory v0.0.0-20201122165311-935a9345ce9b
	gitlab.com/hyperd/trader/middleware v0.0.0-20201122165311-935a9345ce9b
	gitlab.com/hyperd/trader/transport/http v0.0.0-20201122165311-935a9345ce9b
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
