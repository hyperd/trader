package implementation

import (
	"context"
	"database/sql"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/google/uuid"
	"gitlab.com/hyperd/trader"
)

// service implements the Trader Service
type service struct {
	repository trader.Repository
	logger     log.Logger
}

// NewService creates and returns a new Trader service instance
func NewService(rep trader.Repository, logger log.Logger) trader.Service {
	return &service{
		repository: rep,
		logger:     logger,
	}
}

func (s *service) PostTrade(ctx context.Context, trade trader.Trade) (string, error) {
	logger := log.With(s.logger, "method", "PostTrade")
	uuid := uuid.New()

	trade.ID = uuid

	id, err := s.repository.PostTrade(ctx, trade)

	if err != nil {
		level.Error(logger).Log("err", err)
		return id, err
	}
	return id, err
}

func (s *service) GetTrade(ctx context.Context, uuid uuid.UUID) (trader.Trade, error) {
	logger := log.With(s.logger, "method", "GetTrade")
	trade, err := s.repository.GetTrade(ctx, uuid)
	if err != nil {
		level.Error(logger).Log("err", err)
		if err == sql.ErrNoRows {
			return trade, trader.ErrNotFound
		}
		return trade, trader.ErrQueryRepository
	}
	return trade, err
}

func (s *service) GetTrades(ctx context.Context) ([]trader.Trade, error) {
	logger := log.With(s.logger, "method", "GetTrades")
	trades, err := s.repository.GetTrades(ctx)
	if err != nil {
		level.Error(logger).Log("err", err)
		if err == sql.ErrNoRows {
			return nil, trader.ErrNotFound
		}
		return nil, err
	}
	return trades, err
}
